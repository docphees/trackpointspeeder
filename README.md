# TrackpointSpeeder

This script gives access to ThinkPad TrackPoint's speed and sensitivity 
settings, allows setting, saving, loading them and also can install itself 
into /usr/bin and create a desktop entry for autostart at login.

Since the trackpoint values are overwritten at some point after login, this
script solves the race condition by setting the desired values every 5 seconds
for one minute after login. This is not recognisable for the user.

The script can install itself when run as sudo with the -i parameter.

## Info

Author:   docphees 

Version:  0.7.0 

License:  GPL 

Year:     2016

This basically does something like this, just in a nicer way:

    #echo 150 > /sys/devices/platform/i8042/serio1/serio2/speed
    #echo 240 > /sys/devices/platform/i8042/serio1/serio2/sensitivity

## Usage
Download to anywhere and run it with sudo with the -h parameter to get 
this help:

    This sript allows to read and change the speed and sensitivity
    settings of ThinkPad TrackPoint devices.
    The script requires write access to the device and must be run
    with root privileges.
    
    usage: $selfcall [OPTION]
    Selecting an option is mandatory! Options are:
    -g        Prints out the current speed settings
    -s        Sets both speed and sensitivity values.
              Requires two space/comma seperated integer
              arguments from 1 to 255.
              -s SPEED,SENSITIVITY
              Example: -s 100,200
    -S        Saves the current speed and sensitivity
              values.
    -R        Restores a previously saved speed and
              sensitivity values.
    -E        Restores a previously saved speed like -R
              and tries to ensure it stays set.
    -i        Install trackpointspeeder daemon on boot.
    -u        Uninstall trackpointspeeder daemon on boot.
    -h        Print this help text.

Mainly you will be looking for the -s (set), -S (save), -R (restore) commands to
find good settings. Then run it with -i to install and auto-run at login.